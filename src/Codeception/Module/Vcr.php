<?php
namespace Essent\Test\Codeception\Module;

use Codeception\Configuration;
use Codeception\Module as CodeceptionModule;
use Codeception\Step;
use Codeception\Test\Cest;
use Codeception\Test\Unit;
use Codeception\TestInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use VCR\Cassette;
use VCR\Event\AfterPlaybackEvent;
use VCR\Event\BeforeHttpRequestEvent;
use VCR\Event\BeforePlaybackEvent;
use VCR\Event\BeforeRecordEvent;
Use VCR\VCR as PhpVcr;
use VCR\VCREvents;

class Vcr extends CodeceptionModule
{
    /** @var array */
    protected $config = [
        'storage' => 'json', // json | yaml | blackhole
        'record'   => true,
        'playback' => false,
    ];

    /** @var array */
    protected $requiredFields = ['cassettePath', 'storage', 'record', 'playback'];

    /** @var string */
    protected $cassettePath;

    /** @var string */
    protected $cassetteName;

    /** @var array */
    protected $removeWhenDone = [];

    // HOOK: used after configuration is loaded
    public function _initialize()
    {
        if ($this->isRecordingOrPlaying()) {
            $config = PhpVcr::configure();

            $this->cassettePath = realpath(Configuration::projectDir() . $this->config['cassettePath']);
            if ($this->cassettePath === false) {
                throw new \Exception(sprintf('cassettePath "%s" does not exist', $this->config['cassettePath']));
            }

            if (!is_writable($this->cassettePath)) {
                @mkdir($this->cassettePath);

                if (!is_writable($this->cassettePath)) {
                    throw new \Exception(sprintf('cassettePath "%s" is not writeable', $this->config['cassettePath']));
                }
            }

            $config->setCassettePath($this->config['cassettePath']);
            $config->setStorage($this->config['storage']);

            if ($this->config['record'] == false) {
                $config->setMode(PhpVcr::MODE_NONE);
            } else {
                $config->setMode(PhpVcr::MODE_NEW_EPISODES);
            }

            // configure request matching
            $config->addRequestMatcher(
                'custom_headers',
                function (\VCR\Request $first, \VCR\Request $second) {
                    // ignore some headers
                    $ignored = [
                        'User-Agent',
                        'X-LOG-ID',
                        'X-LOG-DESCRIPTION',
                        'X-LOG-COMPONENT',
                        'X-LOG-USER',
                    ];
                    $headers1 = [];
                    foreach ($first->getHeaders() as $key => $value) {
                        if (!in_array($key, $ignored, true)) {
                            $headers1[$key] = $value;
                        }
                    }
                    $headers2 = [];
                    foreach ($second->getHeaders() as $key => $value) {
                        if (!in_array($key, $ignored, true)) {
                            $headers2[$key] = $value;
                        }
                    }

                    // Use array_filter to ignore headers which are null
                    return array_filter($headers1) === array_filter($headers2);
                }
            );
            $config->enableRequestMatchers([
                'method', 'url', 'query_string', 'host', 'body', 'post_fields', 'custom_headers',
            ]);

            if ($this->config['playback'] == false) {
                /** @var EventDispatcher $eventDispatcher */
                $moduleConfig = $this->config;
                $eventDispatcher = PhpVcr::getEventDispatcher();
                $eventDispatcher->addListener(
                    VCREvents::VCR_BEFORE_PLAYBACK,
                    function(BeforePlaybackEvent $event) use ($moduleConfig) {
                        $request = $event->getRequest();
                        $temp = json_encode(['url' => $request->getUrl()]);
                        $request->setUrl($temp);
                    }
                );
                $eventDispatcher->addListener(
                    VCREvents::VCR_BEFORE_HTTP_REQUEST,
                    function(BeforeHttpRequestEvent $event) use ($moduleConfig) {
                        $request = $event->getRequest();
                        $temp = json_decode($request->getUrl());
                        $request->setUrl($temp->url);
                        // do not perform ssl verification for certificates (needed for Windows users)
                        $request->setCurlOption(CURLOPT_SSL_VERIFYPEER, false);
                    }
                );
            }
        }
    }

    // HOOK: on every Guy class initialization
    public function _cleanup()
    {
    }

    // HOOK: before each suite
    public function _beforeSuite($settings = [])
    {
    }

    // HOOK: after suite
    public function _afterSuite()
    {
    }

    // HOOK: before every step
    public function _beforeStep(Step $step)
    {
    }

    // HOOK: after every  step
    public function _afterStep(Step $step)
    {
    }

    // HOOK: before scenario
    public function _before(TestInterface $test)
    {
        if ($this->isRecordingOrPlaying()) {
            /** @var Unit|Cest $test */
            $reportFields = $test->getReportFields();

            $name = str_replace(' ', '-', $reportFields['name']);
            $class = str_replace('\\', '-', $reportFields['class']);

            $this->cassetteName = preg_replace('/[^a-z0-9-]/', '', strtolower($class.'-'.$name));

            PhpVcr::turnOn();
            PhpVcr::insertCassette($this->cassetteName);
        }
    }

    // HOOK: after scenario
    public function _after(TestInterface $test)
    {
        if ($this->isRecordingOrPlaying()) {
            PhpVcr::turnOff();
            $this->removeCassetteIfEmpty();
        }
    }

    // HOOK: on fail
    public function _failed(TestInterface $test, $fail)
    {
        if ($this->isRecordingOrPlaying()) {
            PhpVcr::turnOff();
            $this->removeCassetteIfEmpty();
        }
    }

    /**
     * Removes current cassette if empty
     */
    protected function removeCassetteIfEmpty()
    {
        $cassette = $this->cassettePath . DIRECTORY_SEPARATOR . $this->cassetteName;
        if (file_exists($cassette) && is_file($cassette)) {
            $cassetteContent = file_get_contents($cassette);
            if ($cassetteContent === '[]') {
                $this->removeWhenDone[] = $cassette;
            }
        }
    }

    /**
     * @return boolean
     */
    protected function isRecordingOrPlaying() : bool
    {
        return $this->config['record'] || $this->config['playback'];
    }

    /**
     *
     */
    public function __destruct()
    {
        foreach ($this->removeWhenDone as $toRemove) {
            @unlink($toRemove);
        }
    }
}