<?php
namespace Essent\Test\Codeception\Module;

use Codeception\Lib\Framework;
use Codeception\TestInterface;
use Codeception\Configuration;
use Essent\Test\Codeception\Lib\Connector\Slim as SlimConnector;
use Slim\Container;

/**
 * This module allows you to run tests by routing them into a Slim Framework bootstrap
 *
 * Uses `config/app.php` file by default to load a file that inits the \Slim\App and assigns it to $app
 *
 * ## Status
 *
 * * Stability: **beta**
 *
 * ## Config
 *
 * * container: relative path to file which returns Container (default: `config/container.php`)
 * * basePath: basePath of the api
 *
 * ## API
 *
 * * application -  instance of `\Slim\App`
 * * container - instance of `\Interop\Container\ContainerInterface`
 * * client - BrowserKit client
 */
class Slim extends Framework
{
    /** @var array */
    protected $config = [
        'application' => 'config/app.php',
        'basePath'    => '/api',
    ];

    /**
     * @var SlimConnector
     */
    public $client;

    /**
     * @var \Interop\Container\ContainerInterface
     */
    public $container;

    /**
     * @var \Slim\App
     */
    public $application;

    /**
     * @var array
     */
    private $originalServices = [];

    /**
     *
     */
    public function _initialize()
    {
        $cwd = getcwd();
        chdir(Configuration::projectDir());
        require Configuration::projectDir() . $this->config['application'];
        $this->application = $app;
        chdir($cwd);
    }

    /**
     * @param TestInterface $test
     */
    public function _before(TestInterface $test)
    {
        $this->client = new SlimConnector();
        $this->client->setApplication($this->application);
        $this->client->setBasePath($this->config['basePath']);

        parent::_before($test);
    }

    /**
     * @param TestInterface $test
     */
    public function _after(TestInterface $test)
    {
        $this->restoreContainer();

        //Close the session, if any are open
        if (session_status() === PHP_SESSION_ACTIVE) {
            session_write_close();
        }

        parent::_after($test);
    }

    /**
     * Restores the container to its default state, re-setting all original factories
     */
    public function restoreContainer()
    {
        if (!count($this->originalServices)) {
            return;
        }

        $container = $this->application->getContainer();

        if (!$container instanceof Container) {
            throw new \InvalidArgumentException(sprintf('Container %s is not supported at this time', $container));
        }

        foreach ($container->keys() as $id) {
            $raw = $this->originalServices[$id] ?? $container->raw($id);
            $container->offsetUnset($id);
            $container->offsetSet($id, $raw);
        }

        $this->originalServices = [];
    }

    /**
     * Adds a service into the container, saves the original factory/method and restores it after the test
     *
     * @param string $serviceName
     * @param mixed $service
     *
     * @throws \InvalidArgumentException If the container is not supported.
     * @throws \LogicException If a user tries to overwrite a service definition twice.
     */
    public function setInContainer($serviceName, $service)
    {
        $container = $this->application->getContainer();

        if (!$container instanceof Container) {
            throw new \InvalidArgumentException(sprintf('Container %s is not supported at this time', $container));
        }

        if (!count($this->originalServices)) {
            $this->restoreContainer();
        }

        if ($container->has($serviceName)) {
            if (!isset($this->originalServices[$serviceName])) {
                $this->originalServices[$serviceName] = $container->raw($serviceName);
            }
            $container->offsetUnset($serviceName);
        }

        $container->offsetSet($serviceName, function() use(&$service) {
            return $service;
        });
    }

    /**
     * @param $serviceName
     *
     * @return mixed
     *
     * @throws \InvalidArgumentException If the service definition is not found.
     */
    public function grabFromContainer($serviceName)
    {
        $container = $this->application->getContainer();

        if (!$container->has($serviceName)) {
            throw new \InvalidArgumentException(sprintf('Service %s not found in the container', $serviceName));
        }

        return $container->get($serviceName);
    }
}
