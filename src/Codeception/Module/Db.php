<?php
namespace Essent\Test\Codeception\Module;

use Codeception\Configuration;
use Codeception\Exception\ModuleConfigException;
use Codeception\Exception\ModuleException;
use Codeception\Lib\Driver\Db as Driver;
use Codeception\Module\Db as DbModule;

/**
 * Class Db
 */
class Db extends DbModule
{
    /**
     * @var \PDO
     */
    public $dbh;

    /**
     * @return void
     */
    public function _initialize()
    {
        $this->connect();

        if ($this->config['dump'] && ($this->config['cleanup'] || $this->config['populate'])) {
            $this->readSql();
        }

        // starting with loading dump
        if ($this->config['populate']) {
            if ($this->config['cleanup']) {
                $this->cleanup();
            }
            $this->loadDump();
            $this->populated = true;
        }
    }

    /**
     * @return void
     */
    private function readSql()
    {
        if (!is_array($this->config['dump'])) {
            $this->config['dump'] = [$this->config['dump']];
        }

        // set mysql variables
        $this->sql = [
            'SET autocommit=0;',
            'SET unique_checks=0;',
            'SET foreign_key_checks=0;',
        ];

        // drop all views and tables
        $query = $this->dbh->query('SHOW FULL TABLES');
        while ($table = $query->fetch()) {
            if ($table['Table_type'] === 'VIEW') {
                $this->sql[] = "DROP VIEW {$table[0]};";
            } else {
                $this->sql[] = "DROP TABLE {$table[0]};";
            }
        }

        // read the sql dump files
        foreach ($this->config['dump'] as $dump) {
            $files = glob(Configuration::projectDir() . $dump);
            foreach ($files as $file) {
                $this->sql = array_merge($this->sql, $this->readOneDump($file));
            }
        }

        // add commit statement and reset mysql variables
        $this->sql[] = 'COMMIT;';
        $this->sql[] = 'SET foreign_key_checks=1;';
        $this->sql[] = 'SET unique_checks=1;';
        $this->sql[] = 'SET autocommit=1;';
    }

    /**
     * @return array|null
     *
     * @throws ModuleConfigException In case the passed file doesn't exist.
     */
    protected function readOneDump($file)
    {
        if (!file_exists($file)) {
            throw new ModuleConfigException(
                __CLASS__,
                "\nFile with dump doesn't exist.\n"
                . "Please, check path for sql file: "
                . $file
            );
        }

        $sql = file_get_contents($file);

        // remove C-style comments (except MySQL directives)
        $sql = preg_replace('%/\*(?!!\d+).*?\*/%s', '', $sql);

        if (!empty($sql)) {
            // split SQL dump into lines
            $readSql = preg_split('/\r\n|\n|\r/', $sql, -1, PREG_SPLIT_NO_EMPTY);
        }

        return isset($readSql)? $readSql : null;
    }

    /**
     * @return void
     *
     * @throws ModuleException In case the creation of the connection fails.
     */
    private function connect()
    {
        try {
            $this->driver = Driver::create($this->config['dsn'], $this->config['user'], $this->config['password']);
        } catch (\PDOException $e) {
            $message = $e->getMessage();
            if ($message === 'could not find driver') {
                list ($missingDriver, ) = explode(':', $this->config['dsn'], 2);
                $message = "could not find $missingDriver driver";
            }

            throw new ModuleException(__CLASS__, $message . ' while creating PDO connection');
        }

        $this->dbh = $this->driver->getDbh();
    }
}